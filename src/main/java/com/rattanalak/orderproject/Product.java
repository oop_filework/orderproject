/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rattanalak.orderproject;

import java.util.ArrayList;

/**
 *
 * @author Rattanalak
 */
public class Product {
   private int id;
   private String Name;
   private double price;

    public Product(int id, String Name, double price) {
        this.id = id;
        this.Name = Name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", Name=" + Name + ", price=" + price + '}';
    }
    
    public static ArrayList<Product> getMockProductList(){
       ArrayList<Product> list = new ArrayList<Product>();
       for(int i=0; i<10;i++){
        list.add(new Product(i+1,"Coffee"+i+1,i*5));
    }
       return list;
    }
}
